﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="14008000">
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*'!!!*Q(C=\&gt;7`D2J"&amp;-@RHSU([]13(2CS#^',,C?B!&amp;JY,3"HB,4Q#C#B!YM+,$V&gt;"\2!#^RXBM?#(:D%NE\7T4*\O\_&gt;0R^GFTWJFU([J0/D-PSW=*U7\&amp;L$3_PL556V@&gt;Q.FU&lt;^]WP`OU&lt;8&gt;L?OQ^DS`&amp;0`Y8Q\O85?RPE@_"]5P:@X]H_8T=-@Q6``(8R1?R&amp;2ERJ5JZL;N/=E4`)E4`)E4`)A$`)A$`)A$X)H&gt;X)H&gt;X)H&gt;X)D.X)D.X)D.`+_E9N=Z#+(F%S?4*1-GAS1.);C:*&gt;Y%E`C34S=+P%EHM34?")04:2Y%E`C34S*BWZ+0)EH]33?R-.18:*^)]?4?"B?A3@Q"*\!%XC95I%H!!34"1-(A]"1=$%Y#$S"*`"QK-!4?!*0Y!E]8&amp;&lt;A#4S"*`!%(LLU69GO;2MZ(I;2YX%]DM@R/"['FO.R0)\(]4A?JJ0D=4Q/QJH1'2S#H%Z/!_@%]4A?`MDR/"\(YXA=$Z@[(@+_-EX4.H)]BM@Q'"\$9XA91I&lt;(]"A?QW.Y'&amp;;'R`!9(M.D?*B+BM@Q'"Y$9ET+^$)'-TI;D9T!]0$J4YPVOR2&gt;9HW4[O&amp;60:3KBUXV%+E?$N6.6^V-V5V3,&lt;ZK567,J6I%V:&gt;4I659V33KTKWB4OS0V!.V4^V3V^16&gt;5'&gt;53?N[R^O?$K&gt;&gt;$Q?&gt;4A=N.`PN&gt;VON6[PN6KNN&amp;AM.*P..*F-RN@!6\&lt;RB8"Z,WUYXLX9^^X4@,L\M:TP0H_&lt;RJ@F=]P`Q@`H.`"OV%@&gt;T]%;P1,6]EVR!!!!!!</Property>
	<Property Name="NI.Lib.LocalName" Type="Str">Auto Update Util</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.1.1.3</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6,0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%W.T9S-D5Q0#^797Q_$1I],V5T-DY.#DR6-T)_$1I]4G&amp;N:4Z#97.L:X*P&gt;7ZE)%.P&lt;'^S0#^/97VF0AU+0&amp;:B&lt;$YR.D=X.T)S.4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_-4EV0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$%],UZB&lt;75_$1I]6G&amp;M0D%Z.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!S0#^/97VF0AU+0&amp;:B&lt;$YR/45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_-4EV0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$1],UZB&lt;75_$1I]6G&amp;M0D%Z.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!V0#^/97VF0AU+0&amp;:B&lt;$YR/45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_-4EV0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$=],UZB&lt;75_$1I]6G&amp;M0D%Z.4QP6G&amp;M0AU+0#^6/$Y.#DQP1WRV=X2F=DY.#DR*-49_$1I]4G&amp;N:4Z8;72U;$QP4G&amp;N:4Y.#DR797Q_-4QP6G&amp;M0AU+0#^*-49_$1I]26=_$1I]4G&amp;N:4Z.&lt;W2F0#^/97VF0AU+0%.I&lt;WFD:4Z$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0E^S0#^$;'^J9W5_$1I]1WBP;7.F0E6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z#;81A1WRF98)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^U)%.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^U)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP=C"&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^U)%*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U680AU+0%6-0AU+0%ZB&lt;75_5X2Z&lt;'5],UZB&lt;75_$1I]1WBP;7.F0F.P&lt;'FE0#^$;'^J9W5_$1I]1WBP;7.F0E2B=WA],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'^U0#^$;'^J9W5_$1I]1WBP;7.F0E2B=WAA2'^U0#^$;'^J9W5_$1I]1WBP;7.F0E2B=WAA2'^U)%2P&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_2GFM&lt;#"3&gt;7RF0#^/97VF0AU+0%.I&lt;WFD:4Z&amp;&gt;G6O)%^E:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z8;7ZE;7ZH0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z&amp;&lt;G1A1W&amp;Q=TQP4G&amp;N:4Y.#DR$;'^J9W5_2'6G986M&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z'&lt;'&amp;U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I],U.M&gt;8.U:8)_$1I!!!!!</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6%0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%W.T1U.$1Y0#^797Q_$1I],V5T-DY.#DR6-T)_$1I]4G&amp;N:4Z#97.L:X*P&gt;7ZE)%.P&lt;'^S0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5T-DY.#DR$&lt;(6T&gt;'6S0AU+0%ZB&lt;75_2GFM&lt;#"1982U:8*O0#^/97VF0AU+0%ZV&lt;56M&gt;(-_/$QP4H6N27RU=TY.#DR6/$Y.#DR/97VF0F*P&gt;S!Q0#^/97VF0AU+0&amp;:B&lt;$YR-D9],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-4QP4G&amp;N:4Y.#DR797Q_-4)W0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$)],UZB&lt;75_$1I]6G&amp;M0D%S.DQP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!T0#^/97VF0AU+0&amp;:B&lt;$YR-D9],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.$QP4G&amp;N:4Y.#DR797Q_-4)W0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$5],UZB&lt;75_$1I]6G&amp;M0D%S.DQP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!W0#^/97VF0AU+0&amp;:B&lt;$YR-D9],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.TQP4G&amp;N:4Y.#DR797Q_-4)W0#^797Q_$1I],V5Y0AU+0#^$&lt;(6T&gt;'6S0AU+0%ER.DY.#DR/97VF0F&gt;J:(2I0#^/97VF0AU+0&amp;:B&lt;$YT0#^797Q_$1I],UER.DY.#DR&amp;6TY.#DR/97VF0EVP:'5],UZB&lt;75_$1I]1WBP;7.F0E.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0E*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^S)%6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP26=_$1I]25Q_$1I]4G&amp;N:4Z4&gt;(FM:4QP4G&amp;N:4Y.#DR$;'^J9W5_5W^M;71],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1A2'^U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z';7RM)&amp;*V&lt;'5],UZB&lt;75_$1I]1WBP;7.F0E6W:7YA4W2E0#^$;'^J9W5_$1I]1WBP;7.F0F&gt;J&lt;G2J&lt;G=],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E6O:#"$98"T0#^/97VF0AU+0%.I&lt;WFD:4Z%:7:B&gt;7RU0#^$;'^J9W5_$1I]1WBP;7.F0E:M981],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DQP1WRV=X2F=DY.#A!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!";N5F.31QU+!!.-6E.$4%*76Q!!%GA!!!2&amp;!!!!)!!!%EA!!!!F!!!!!3";?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=Q!!!!!!!+!5!)!!!$!!!#A!"!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!E/`L4(@H/UWYSQ?I'T)''A!!!!Q!!!!1!!!!"&amp;PNV5U2I1*(H&gt;P&gt;F)&lt;4H$P5(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!#4W^QN.Z9,4,8?6Q,`@[!A!1!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!%"C5TN&amp;WE-%9EXA(/)H[WS!!!!!%!!!!!!!!!#=!!5R71U-!!!!"!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!-!!!!!!A!"!!!!!!!7!!!!#HC=9W"G9"*A%'!59!!!!09!.Q!!!!!!(Q!!!-RYH'.AQ!`_!Q%"*41(".T!$-1M1-Q+!%(J#!5!!!!!$!!"6EF%5Q!!!!!!!Q!!!!Q5!)!!!!!%-41O-!!!!!!-&amp;!#!!!!!"$%U,D!!!!!!$"1!A!!!!!1R.#YQ!!!!!!Q5!)!!!!!%-41O-!!!!!!-&amp;!#!!!!!"$%U,D!!!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A'!!!9'9!!''"A!"G!'!!;!!1!'Q!-!"L!.!!;-/Q!'A^5!"I#L!!;!V1!'A+M!"I$6!!;!KQ!'A.5!"G#O!!99W!!'"O!!"A'!!!@````]!!!1!````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!#QM!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!#[V@C;U,!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!#[V@.45V.9GN#Q!!!!!!!!!!!!!!!!!!!!$``Q!!#[V@.45V.45V.47*L1M!!!!!!!!!!!!!!!!!!0``!)F@.45V.45V.45V.45VC;U!!!!!!!!!!!!!!!!!``]!8V]V.45V.45V.45V.48_C1!!!!!!!!!!!!!!!!$``Q"@C9F@.45V.45V.48_`PZ@!!!!!!!!!!!!!!!!!0``!&amp;_*C9G*8T5V.48_`P\_`F]!!!!!!!!!!!!!!!!!``]!8YG*C9G*C6_N`P\_`P\_8Q!!!!!!!!!!!!!!!!$``Q"@C9G*C9G*C@\_`P\_`PZ@!!!!!!!!!!!!!!!!!0``!&amp;_*C9G*C9G*`P\_`P\_`F]!!!!!!!!!!!!!!!!!``]!8YG*C9G*C9H_`P\_`P\_8Q!!!!!!!!!!!!!!!!$``Q"@C9G*C9G*C@\_`P\_`PZ@!!!!!!!!!!!!!!!!!0``!&amp;_*C9G*C9G*`P\_`P\_`F]!!!!!!!!!!!!!!!!!``]!C9G*C9G*C9H_`P\_`P[*C1!!!!!!!!!!!!!!!!$``Q!!8V_*C9G*C@\_`P[*L6]!!!!!!!!!!!!!!!!!!0``!!!!!&amp;_*C9G*`P[*C6]!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!"@C9G*C45!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!8T5!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!!)!!1!!!!!!$!!"2F")5!!!!!!!!Q!!!!1!!!!!!!!&amp;C1!!%NBYH.W97UQ=:24(TT=---N&amp;:J&gt;,W92GFX7Y;%52,[WEVCKT7*1#&amp;4$9;NM.OQ75SX)JVM15UWT1.7H3W)1(9V^Y)#9GGA;.]=H'&lt;+NR(_R$N9E]L/7BM:I;'E-QWNHR@.`M\/T/8FQ*&lt;9Q]4#&lt;,&gt;]\ZPP0\`]`-,I$NCFD.2?'E!E2=RZN/"9K]91)1;B)A^F=`$W)`_1N)B:UIM&amp;@I&amp;V?Y++F2I-1&lt;LB?;Z3#MY7LV*`5^=J7L%+`BUA,2DMG+&amp;#DTBL&gt;:HZ-CIP2*D24-V\/719VYGE3Z!=GR):Q/4'""#$43K\7*2)()N4Q@=06YRHU"C8ZK;2,M,+6&amp;!6%/FUZ*E1&lt;-C+7`9CGZ38+&gt;/[7H"%T:#-P,SU;161OK:^P9B4(E/A$:Y#;TR.DE]0:J+@)!CSFC-6BH1[]D/[*H[&gt;ZJE$GU8![,')JR*W*(JL&amp;[H,+8R;WOLG)=8G.RQQJ53*&amp;H",NQT@ND@OG"Q/&gt;!A)3/#/L&lt;KJ&gt;LY3;N8:1#7WN$%(7N2/T&amp;_VY&amp;\A_%O;0![\7_A&amp;M-![^DW)U93$P$9*'$)BA=TPQ$"ZE0O&lt;;XD2[@HP&amp;./3?//1&gt;(0&gt;044P`5S+RHRO@U?G9]K93?F-/7H@4UN"A4"Z1$4T[$GYH&gt;HI#FJ36M!&amp;[.U$U9OEW+R/0+9K@2/_YV/E[L'JV\#DMH`^%X2,N8V]LJGMVHGH7T[S#\$L$L1_Q[&lt;GCZ"&lt;6M&amp;=X&lt;4&gt;,S9VOPZ=&gt;29C&gt;.7I9&lt;:"(_T+,,H6J1AJ98M5P,=#.,T#[-G5P3]C,77&gt;&lt;L:.0S%[F;8N2LR&lt;7]M,#1&amp;)&gt;%7O.;ZAH2N"SYL.Z7&lt;V.&amp;@["_S^V$'JCC]VH%)913HTTHM9A,C^$_&amp;_L^&lt;]MIYL(M!*Q&amp;I!GZYGG`@X2EU$-T-D(O&gt;!_YH4W?G7%4&amp;3QEIQB&lt;J%BRAHZN5!6_O++@7L7S5``][UU]N;KK2K&amp;!,&lt;S&amp;P#8[;4RHC1,N/)!?F3)\N,V@I$GNW***+-=]/ZI@Q4Q@@P3RV9%(ZE1\C?9Z1Q\N?%@"&amp;H#S*&lt;6UC?D3DLIOO'DWF*8M^,8UKKX%T2&lt;4J2;(A@2:/:S0"C7JG^%&lt;O=\/&amp;^^8,,2&lt;A8X?=)%&amp;Z)CD.Y'#&amp;;\#@4&amp;=]:H43_X3T8"6RB;KP[F265&amp;(:=@&amp;-6S3#URI3B8IQ`*W&amp;(]6*J7#:&lt;3Y$"TU1F(3;-EEA93=UI.AU/F(YD9J5KHDJH`6W%UT'D[ZY&gt;5ZIUF=/49WFO0+T??-UU9DPIB'L(M*[AY#$MB@N!':BQ.S`D".9;Q&lt;3VSX;KDFF'EG7[%3J@*^MF2QKZI6N0R]`8S?&gt;1VX_S\PY*WI2Q[.\3LJ[&gt;P8D$PH[@&lt;Z?XH*MB9LA_Y@V^W0TA&gt;SVOR_@U&lt;X"X/35]B6X?-:@-UTZ(0W_`'ZB9_TT".A=J-4)*0\J`Z,\J`?P0NH=H&lt;`\*VQ`_NXRPUH`M`O@S/._`04O0`.$/[@WY4\#`[F_Q`ASRH.\=&amp;`"/&amp;#]GO8,[0R&gt;_&gt;I@%O\T_&gt;V&gt;HH'T'^AR1I=E].W@&amp;-JQ_23M)4GLE)PO+!Q4H]FMGL1JY\;_%:DQ?[`4LC`F("`-9%#FBH#-HOES+T*=QWM$F&lt;9EDL9L/(5Q;86_&gt;)UO*#6;8#^ET+YE0[)C&lt;[)\7N%1JHI[WJ[.6&amp;.&amp;T66&amp;+)K$/!$.&amp;U1UZUX!T_9%@D_(.`T3P2*HZ\ZI&lt;P$`/7\R0S6,7:_?*0-D[2B,C1R&gt;^/HQ#V-ZT%T@TYD]Y&gt;T.,H!4.\`1E=K\]Y5XKX9H`;NZLU`0?_*L?&lt;&gt;F:\XR':Z&gt;[@B\=_"&gt;U];XB&lt;'W^+6]$Q76CY&gt;D`_7*02+MJJNA?D'Z(-+6-JNNO`%$OR6C&gt;!BO-6T_$5&gt;P\&amp;8#??%^&gt;!0]6_&lt;1JXRS-"F_*X\6(L@)64`$&gt;9\0*1!!!!!!!!%!!!!71!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!!1!!!!!!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!"!=!!!!)!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=B1!A!!!!!!"!!A!-0````]!!1!!!!!!L!!!!!9!%E!Q`````QB':76E)&amp;6331!!%E!Q`````QF':76E)%ZB&lt;75!&amp;E!Q`````QR197.L97&gt;F)%ZB&lt;75!!"Z!-P````]518"Q&lt;'FD982J&lt;WYA26B&amp;)&amp;"B&gt;'A!!#*!-P````]95'&amp;D;W&amp;H:3"6='2B&gt;'6S)%6923"1982I!!!K1&amp;!!"1!!!!%!!A!$!!197HFB;#""&gt;82P)&amp;6Q:'&amp;U:3"6&gt;'FM;82Z!!!"!!5!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N="1!A!!!!!!"!!5!"Q!!!1!!XHIM7!!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!5!)!!!!!!!1!&amp;!!=!!!%!!.Z[,&amp;A!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D&amp;!#!!!!!!!%!#!!Q`````Q!"!!!!!!#M!!!!"A!31$$`````#%:F:71A66**!!!31$$`````#5:F:71A4G&amp;N:1!71$$`````$&amp;"B9WNB:W5A4G&amp;N:1!!(E!S`````R2"=("M;7.B&gt;'FP&lt;C"&amp;7%5A5'&amp;U;!!!)E!S`````RB197.L97&gt;F)&amp;6Q:'&amp;U:8)A26B&amp;)&amp;"B&gt;'A!!#J!5!!&amp;!!!!!1!#!!-!""B;?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(E!!!%!"1!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF&amp;!#!!!!!!!%!"1!$!!!"!!!!!!!5!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U921!A!!!!!!'!"*!-0````])2G6F:#"65EE!!"*!-0````]*2G6F:#"/97VF!":!-0````]-5'&amp;D;W&amp;H:3"/97VF!!!?1$,`````&amp;%&amp;Q='RJ9W&amp;U;7^O)%6923"1982I!!!C1$,`````'&amp;"B9WNB:W5A68"E982F=C"&amp;7%5A5'&amp;U;!!!+E"1!!5!!!!"!!)!!Q!%'&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?1!!!1!&amp;!!!!!!!!!!!!!!!!5&amp;2)-!!!!!1!!!!!5&amp;2)-!!!!!1!!!!!!!!!!!!!!"2/33Z-6CZ"&lt;'QO5W^V=G.F4WZM?21!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=B1!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!"1!!!!!!!!!"!!!!!A!!!!-!!!!%!!!!!!!!!!!%!!E!"1!!!!1!!!##!!!!+!!!!!)!!!1!!!!!"1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$H!!!":HC=F:$";M*!&amp;%7PGL2;Y[:)=&gt;(+V(5*ID]1E1BO*!A"[?[2D$JU.-'-5POR`1&lt;\"@JC&amp;.SY[-RC?'@/P1-$I)&amp;HLXPE62V*'9NQ/M;6V-ZE1CO*FY)Y!56@N*!&amp;2.PLZ&lt;1Z3&amp;/N)D)K71N`ZIO!T",I&amp;,?N;S:-9T*S=W.]Y_`XZW-'I#Q_^\15A[V*,JY)D&gt;,+\&amp;W^CT2FW?N&gt;)4);03_!T4UFF&amp;'"^4&lt;5WSR`+ZG,=VKE'\8,1ZQE.NF^1MT_!1]]6#YD_KD#YN0^6Z_&amp;&gt;`Z('Y_I=5_&gt;N]-JBXG?RQFW4W3&amp;!!!!!'Q!!1!#!!-!"!!!!%A!$Q1!!!!!$Q$:!.1!!!"2!!]%!!!!!!]!W1$5!!!!7A!0"!!!!!!0!.E!V!!!!'-!$A1!!!!!$1$5!-1)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65F35V*$$1I!!UR71U.-1F:8!!!3;!!!"%5!!!!A!!!33!!!!!!!!!!!!!!!)!!!!$1!!!1I!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!!!!!"W%2'2&amp;-!!!!!!!!"\%R*:(-!!!!!!!!#!(:F=H-!!!!%!!!#&amp;%&gt;$5&amp;)!!!!!!!!#?%F$4UY!!!!!!!!#D'FD&lt;$A!!!!!!!!#I%.11T)!!!!!!!!#N%R*:H!!!!!!!!!#S%:128A!!!!!!!!#X%:13')!!!!!!!!#]%:15U5!!!!!!!!$"&amp;:12&amp;!!!!!!!!!$'%R*9G1!!!!!!!!$,%*%28A!!!!!!!!$1%*%3')!!!!!!!!$6%*%5U5!!!!!!!!$;&amp;:*6&amp;-!!!!!!!!$@%253&amp;!!!!!!!!!$E%V6351!!!!!!!!$J%B*5V1!!!!!!!!$O&amp;:$6&amp;!!!!!!!!!$T%:515)!!!!!!!!$Y!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!,!!!!!!!!!!!`````Q!!!!!!!!$1!!!!!!!!!!$`````!!!!!!!!!/1!!!!!!!!!!0````]!!!!!!!!!\!!!!!!!!!!!`````Q!!!!!!!!%9!!!!!!!!!!$`````!!!!!!!!!3!!!!!!!!!!!0````]!!!!!!!!"0!!!!!!!!!!!`````Q!!!!!!!!&amp;A!!!!!!!!!!4`````!!!!!!!!!8!!!!!!!!!!"`````]!!!!!!!!"A!!!!!!!!!!)`````Q!!!!!!!!'1!!!!!!!!!!H`````!!!!!!!!!;!!!!!!!!!!#P````]!!!!!!!!"M!!!!!!!!!!!`````Q!!!!!!!!(!!!!!!!!!!!$`````!!!!!!!!!&gt;1!!!!!!!!!!0````]!!!!!!!!#7!!!!!!!!!!!`````Q!!!!!!!!:=!!!!!!!!!!$`````!!!!!!!!"G1!!!!!!!!!!0````]!!!!!!!!'&gt;!!!!!!!!!!!`````Q!!!!!!!!:]!!!!!!!!!!$`````!!!!!!!!$!Q!!!!!!!!!!0````]!!!!!!!!-&amp;!!!!!!!!!!!`````Q!!!!!!!!Q=!!!!!!!!!!$`````!!!!!!!!$#Q!!!!!!!!!!0````]!!!!!!!!-.!!!!!!!!!!!`````Q!!!!!!!!S=!!!!!!!!!!$`````!!!!!!!!$+1!!!!!!!!!!0````]!!!!!!!!1M!!!!!!!!!!!`````Q!!!!!!!"#Y!!!!!!!!!!$`````!!!!!!!!%-!!!!!!!!!!!0````]!!!!!!!!1\!!!!!!!!!#!`````Q!!!!!!!"(9!!!!!"R;?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!3";?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!!!!5!!1!!!!!!!!!!!!!"!#"!5!!!'&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?1!!!1!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!#!!A!!!!!!!!!!!!!!!!!!"!!!!!!!"!!!!!!5!%E!Q`````QB':76E)&amp;6331!!&amp;E!Q`````QR197.L97&gt;F)%ZB&lt;75!!"*!-0````]*2G6F:#"/97VF!#*!-P````]95'&amp;D;W&amp;H:3"6='2B&gt;'6S)%6923"1982I!!"W!0(??CFK!!!!!C";?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=RR;?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO9X2M!$"!5!!%!!!!!1!#!!-&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!%!!!!"0````````````````````]!!!!!!!!!!!!!!!"16%AQ!!!!"!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!)!#!!!!!!!!!!!!!!!!!!!%!!!!!!!)!!!!!"A!31$$`````#%:F:71A66**!!!71$$`````$&amp;"B9WNB:W5A4G&amp;N:1!!%E!Q`````QF':76E)%ZB&lt;75!(E!S`````R2"=("M;7.B&gt;'FP&lt;C"&amp;7%5A5'&amp;U;!!!)E!S`````RB197.L97&gt;F)&amp;6Q:'&amp;U:8)A26B&amp;)&amp;"B&gt;'A!!(A!]&gt;Z[+PI!!!!#)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T(&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZD&gt;'Q!-E"1!!5!!!!"!!)!!Q!%(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!"1!!!!5!!!!!!!!!!1!!!!,`````!!!!!Q!!!!!!!!!!!!!!!&amp;"53$!!!!!%!!!!!&amp;"53$!!!!!%!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!A!)!!!!!!!!!!!!!!!!!!!1!!!!!!!Q!!!!!'!"*!-0````])2G6F:#"65EE!!"*!-0````]*2G6F:#"/97VF!":!-0````]-5'&amp;D;W&amp;H:3"/97VF!!!?1$,`````&amp;%&amp;Q='RJ9W&amp;U;7^O)%6923"1982I!!!C1$,`````'&amp;"B9WNB:W5A68"E982F=C"&amp;7%5A5'&amp;U;!!!?!$RXHIM7!!!!!)A7HFB;#""&gt;82P)&amp;6Q:'&amp;U:3"6&gt;'FM;82Z,GRW9WRB=X-=7HFB;#""&gt;82P)&amp;6Q:'&amp;U:3"6&gt;'FM;82Z,G.U&lt;!!S1&amp;!!"1!!!!%!!A!$!!1&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!&amp;!!!!"1!!!!!!!!!#!!!!!1!!!!-!!!!%!!!!!!!!!!!!!!!!5&amp;2)-!!!!!1!!!!!5&amp;2)-!!!!!1!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!#!!A!!!!!!!!!!!!!!!!!!"!!%!!1!$!!!!!!9!%E!Q`````QB':76E)&amp;6331!!%E!Q`````QF':76E)%ZB&lt;75!&amp;E!Q`````QR197.L97&gt;F)%ZB&lt;75!!"Z!-P````]518"Q&lt;'FD982J&lt;WYA26B&amp;)&amp;"B&gt;'A!!#*!-P````]95'&amp;D;W&amp;H:3"6='2B&gt;'6S)%6923"1982I!!"Y!0(??CR9!!!!!C";?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=RR;?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO9X2M!$*!5!!&amp;!!!!!1!#!!-!""V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!5!!!!"`````A!!!!!!!!!!!!!!!&amp;"53$!!!!!%!!!!!&amp;"53$!!!!!%!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!A!)!!!!!!!!!!!!!!!!</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI_IconEditor" Type="Str">50 48 48 49 56 48 48 48 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 4 47 1 100 0 0 80 84 72 48 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 3 189 0 0 0 0 0 0 0 0 0 0 3 162 0 40 0 0 3 156 0 0 3 96 0 0 0 0 0 9 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 0 0 0 0 0 0 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 0 0 0 0 0 0 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 0 0 0 0 0 0 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 0 0 0 0 0 0 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 0 0 0 0 0 0 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 0 0 0 0 0 0 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 255 197 138 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 7 86 73 32 73 99 111 110 100 1 0 0 0 0 0 6 117 112 100 97 116 101 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 1 8 1 1

</Property>
	<Item Name="Zyah Auto Update Utility.ctl" Type="Class Private Data" URL="Zyah Auto Update Utility.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Accessors" Type="Folder">
		<Item Name="Application EXE Path" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Application EXE Path</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Application EXE Path</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Write Application EXE Path.vi" Type="VI" URL="../Write Application EXE Path.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;[!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%J!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!=7HFB;#""&gt;82P)&amp;6Q:'&amp;U:3"6&gt;'FM;82Z)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!?1$,`````&amp;%&amp;Q='RJ9W&amp;U;7^O)%6923"1982I!!")1(!!(A!!)C";?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=Q!!'VJZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!3!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115684864</Property>
			</Item>
		</Item>
		<Item Name="Feed Name" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Feed Name</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Feed Name</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Write Feed Name.vi" Type="VI" URL="../Write Feed Name.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;O!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%J!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!=7HFB;#""&gt;82P)&amp;6Q:'&amp;U:3"6&gt;'FM;82Z)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!31$$`````#5:F:71A4G&amp;N:1")1(!!(A!!)C";?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=Q!!'VJZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!)1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115684864</Property>
			</Item>
		</Item>
		<Item Name="Feed URI" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Feed URI</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Feed URI</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Write Feed URI.vi" Type="VI" URL="../Write Feed URI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;O!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%J!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!=7HFB;#""&gt;82P)&amp;6Q:'&amp;U:3"6&gt;'FM;82Z)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!31$$`````#%:F:71A66**!!")1(!!(A!!)C";?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=Q!!'VJZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!)1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115684864</Property>
			</Item>
		</Item>
		<Item Name="Package Name" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Package Name</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Package Name</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Write Package Name.vi" Type="VI" URL="../Write Package Name.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;S!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%J!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!=7HFB;#""&gt;82P)&amp;6Q:'&amp;U:3"6&gt;'FM;82Z)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;"B9WNB:W5A4G&amp;N:1!!3%"Q!"Y!!#)A7HFB;#""&gt;82P)&amp;6Q:'&amp;U:3"6&gt;'FM;82Z,GRW9WRB=X-!!"N;?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EA;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!#%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115684864</Property>
			</Item>
		</Item>
		<Item Name="Package Updater EXE Path" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Package Updater EXE Path</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Package Updater EXE Path</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Write Package Updater EXE Path.vi" Type="VI" URL="../Write Package Updater EXE Path.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;_!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%J!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!=7HFB;#""&gt;82P)&amp;6Q:'&amp;U:3"6&gt;'FM;82Z)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!C1$,`````'&amp;"B9WNB:W5A68"E982F=C"&amp;7%5A5'&amp;U;!!!3%"Q!"Y!!#)A7HFB;#""&gt;82P)&amp;6Q:'&amp;U:3"6&gt;'FM;82Z,GRW9WRB=X-!!"N;?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EA;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%A!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115684864</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="API" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Setup" Type="Folder">
			<Item Name="Construct.vi" Type="VI" URL="../Construct.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*N!!!!%!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%2!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!8186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!"Z!-P````]518"Q&lt;'FD982J&lt;WYA26B&amp;)&amp;"B&gt;'A!!#*!-P````]95'&amp;D;W&amp;H:3"6='2B&gt;'6S)%6923"1982I!!"5!0%!!!!!!!!!!C";?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=R*&amp;7%5A37ZG&lt;S"$&lt;(6T&gt;#ZD&gt;'Q!'%"1!!)!"Q!)#%6923"*&lt;G:P!!!31$$`````#%:F:71A66**!!!31$$`````#5:F:71A4G&amp;N:1!71$$`````$&amp;"B9WNB:W5A4G&amp;N:1!!6Q$R!!!!!!!!!!)A7HFB;#""&gt;82P)&amp;6Q:'&amp;U:3"6&gt;'FM;82Z,GRW9WRB=X-42G6F:#"*&lt;G:P)%.M&gt;8.U,G.U&lt;!!;1&amp;!!!Q!+!!M!$!F':76E)%FO:G]!2%"Q!"Y!!#)A7HFB;#""&gt;82P)&amp;6Q:'&amp;U:3"6&gt;'FM;82Z,GRW9WRB=X-!!":"&gt;82P)&amp;6Q:'&amp;U:3"6&gt;'FM;82Z)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!*!!U!$A-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!"!!!!!1!!!!!A!!$1!!!!Q!!!!!!!!!!!!!!1!0!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Update App Info.vi" Type="VI" URL="../Update App Info.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!')!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%2!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!8186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!#*!-P````]95'&amp;D;W&amp;H:3"6='2B&gt;'6S)%6923"1982I!!!?1$,`````&amp;%&amp;Q='RJ9W&amp;U;7^O)%6923"1982I!!"%1(!!(A!!)C";?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=Q!!&amp;E&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EA;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!%A!!!")!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
			</Item>
			<Item Name="Update Feed Info.vi" Type="VI" URL="../Update Feed Info.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'#!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%2!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!8186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"P&gt;81!&amp;E!Q`````QR197.L97&gt;F)%ZB&lt;75!!":!5!!$!!!!!1!##'6S=G^S)'FO!!!31$$`````#5:F:71A4G&amp;N:1!31$$`````#%:F:71A66**!!"%1(!!(A!!)C";?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=Q!!&amp;E&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EA;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"A!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!B!!!!!)!!!#%!!!!B!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
		</Item>
		<Item Name="Updater CLI" Type="Folder">
			<Item Name="Construct from CLI.vi" Type="VI" URL="../Construct from CLI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%\!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%2!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!8186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!%2!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!7186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
			</Item>
			<Item Name="Create Command for Updater EXE.vi" Type="VI" URL="../Create Command for Updater EXE.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"R!-0````]31W^N&lt;7&amp;O:#"-;7ZF)%FO=(6U!!"%1(!!(A!!)C";?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=Q!!&amp;U&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EA&lt;X6U!":!5!!$!!!!!1!##'6S=G^S)'FO!!"%1(!!(A!!)C";?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=Q!!&amp;E&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EA;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
			</Item>
			<Item Name="Launch Updater EXE.vi" Type="VI" URL="../Launch Updater EXE.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%\!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%2!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!8186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!%2!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!7186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
		</Item>
		<Item Name="Check for Package Update.vi" Type="VI" URL="../Check for Package Update.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'1!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!)2"1=G^D:76E)(=P68"E982F!!"%1(!!(A!!)C";?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=Q!!&amp;U&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EA&lt;X6U!"R!)2:1=G^N=(1A&gt;']A1W^O&gt;'FO&gt;75A+&amp;1J!!!71&amp;!!!Q!!!!%!!ABF=H*P=C"J&lt;A!!&amp;E!Q`````QV6='2B&gt;'5A5(*P&lt;8"U!%2!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!7186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"Q!%!!A!"!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!)!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1109918224</Property>
		</Item>
		<Item Name="Launch App EXE.vi" Type="VI" URL="../Launch App EXE.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%\!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%2!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!8186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!%2!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!7186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="Register NIPM Feed.vi" Type="VI" URL="../Register NIPM Feed.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%2!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!8186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!%2!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!7186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074536980</Property>
		</Item>
		<Item Name="Update NI Package.vi" Type="VI" URL="../Update NI Package.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;A!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%2!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!8186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"P&gt;81!'%!B%F*F=X6M&gt;(-A2'FB&lt;'^H)#B5+1!!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!%2!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!7186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"A!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!)!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
		</Item>
		<Item Name="Wait for App Shutdown.vi" Type="VI" URL="../Wait for App Shutdown.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;N!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!)1V03S"U&lt;S"1=G^D:76E!%2!=!!?!!!C)&amp;JZ97AA186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3ZM&gt;G.M98.T!!!8186U&lt;S"6='2B&gt;'5A682J&lt;'FU?3"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!".!#A!.6'FN:7^V&gt;#!I-T"T+1"%1(!!(A!!)C";?7&amp;I)%&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=Q!!&amp;E&amp;V&gt;']A68"E982F)&amp;6U;7RJ&gt;(EA;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!"!!*!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
	</Item>
	<Item Name="Controls" Type="Folder">
		<Item Name="EXE Info Clust.ctl" Type="VI" URL="../EXE Info Clust.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#4!!!!!Q!?1$,`````&amp;%&amp;Q='RJ9W&amp;U;7^O)%6923"1982I!!!C1$,`````'&amp;"B9WNB:W5A68"E982F=C"&amp;7%5A5'&amp;U;!!!3Q$R!!!!!!!!!!)A7HFB;#""&gt;82P)&amp;6Q:'&amp;U:3"6&gt;'FM;82Z,GRW9WRB=X-*1W^O&gt;(*P&lt;#!U!"B!5!!#!!!!!1B&amp;7%5A37ZG&lt;Q!!!1!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
		<Item Name="Feed Info Clust.ctl" Type="VI" URL="../Feed Info Clust.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#0!!!!"!!31$$`````#%:F:71A66**!!!71$$`````$&amp;"B9WNB:W5A4G&amp;N:1!!%E!Q`````QF':76E)%ZB&lt;75!41$R!!!!!!!!!!)A7HFB;#""&gt;82P)&amp;6Q:'&amp;U:3"6&gt;'FM;82Z,GRW9WRB=X-*1W^O&gt;(*P&lt;#!T!"J!5!!$!!!!!1!##5:F:71A37ZG&lt;Q!"!!-!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
	</Item>
	<Item Name="SubVIs" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Is Process Running.vi" Type="VI" URL="../Is Process Running.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$B!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1B3&gt;7ZO;7ZH0Q!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;"S&lt;W.F=X-A4G&amp;N:1!!6!$Q!!Q!!Q!%!!5!"!!%!!1!"!!%!!9!"!!(!!1$!!"Y!!!.#!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!"%A!!!!!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074266640</Property>
		</Item>
	</Item>
</LVClass>
