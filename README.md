# Zyah Auto Update Utility

Code that wraps NIPM API calls into automatic app update functionality. Written in LabVIEW 2020 SP1 (32-bit)

# Helpful Links

- VI Package: https://www.vipm.io/package/zyah_solutions_lib_zyah_auto_update_utility/
- NIPM Feed Manager: https://www.vipm.io/package/national_instruments_lib_nipm_feed_manager_(beta)/
- NIPM API (included in VIPC): https://www.vipm.io/package/national_instruments_lib_nipm_api_(beta)/
- Blog post: https://www.zyahsolutions.com/blog/auto-update-utility
