﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="AF.ActorPaths" Type="Str"></Property>
		<Property Name="AF.Actors" Type="Str"></Property>
		<Property Name="AF.PPL" Type="Path">/C/Projects/Auto Update Example/bin/PPLs/Base Actors/Actor Framework.lvlibp</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="bin (PPLs)" Type="Folder">
			<Item Name="Base Actors" Type="Folder">
				<Item Name="Actor Framework.lvlibp" Type="LVLibp" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp">
					<Item Name="Messages" Type="Folder">
						<Item Name="Message.lvclass" Type="LVClass" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Message/Message.lvclass"/>
						<Item Name="Stop Msg.lvclass" Type="LVClass" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Stop Msg/Stop Msg.lvclass"/>
						<Item Name="Last Ack.lvclass" Type="LVClass" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/LastAck/Last Ack.lvclass"/>
						<Item Name="Launch Nested Actor Msg.lvclass" Type="LVClass" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Launch Nested Actor Msg/Launch Nested Actor Msg.lvclass"/>
					</Item>
					<Item Name="Time-Delayed Send Message" Type="Folder">
						<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
						<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
						<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
					</Item>
					<Item Name="Actor.lvclass" Type="LVClass" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Actor/Actor.lvclass"/>
					<Item Name="Message Priority Queue.lvclass" Type="LVClass" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Message Priority Queue/Message Priority Queue.lvclass"/>
					<Item Name="Message Enqueuer.lvclass" Type="LVClass" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Message Enqueuer/Message Enqueuer.lvclass"/>
					<Item Name="Message Dequeuer.lvclass" Type="LVClass" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Message Dequeuer/Message Dequeuer.lvclass"/>
					<Item Name="Message Queue.lvclass" Type="LVClass" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Message Queue/Message Queue.lvclass"/>
					<Item Name="Init Actor Queues FOR TESTING ONLY.vi" Type="VI" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Actor/Init Actor Queues FOR TESTING ONLY.vi"/>
					<Item Name="Batch Msg.lvclass" Type="LVClass" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Batch Msg/Batch Msg.lvclass"/>
					<Item Name="Reply Msg.lvclass" Type="LVClass" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Reply Msg/Reply Msg.lvclass"/>
					<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
					<Item Name="Self-Addressed Msg.lvclass" Type="LVClass" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Self-Addressed Msg/Self-Addressed Msg.lvclass"/>
					<Item Name="AF Debug.lvlib" Type="Library" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/resource/AFDebug/AF Debug.lvlib"/>
					<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/Utility/High Resolution Relative Seconds.vi"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="Get LV Class Name.vi" Type="VI" URL="../bin/PPLs/Base Actors/Actor Framework.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Name.vi"/>
				</Item>
			</Item>
			<Item Name="Component Actors" Type="Folder">
				<Item Name="Package Update Check Actor.lvlibp" Type="LVLibp" URL="../bin/PPLs/Component Actors/Package Update Check Actor.lvlibp">
					<Item Name="Messages for this Actor" Type="Folder"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../bin/PPLs/Component Actors/Package Update Check Actor.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="Get Command Line Arguments.vi" Type="VI" URL="../bin/PPLs/Component Actors/Package Update Check Actor.lvlibp/1abvi3w/vi.lib/Utility/Get Command Line Arguments.vi"/>
					<Item Name="Get System Directory.vi" Type="VI" URL="../bin/PPLs/Component Actors/Package Update Check Actor.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
					<Item Name="NIPM_API.lvlib" Type="Library" URL="../bin/PPLs/Component Actors/Package Update Check Actor.lvlibp/1abvi3w/vi.lib/National Instruments/NIPM API (Beta)/NIPM_API.lvlib"/>
					<Item Name="Package Update Check Actor.lvclass" Type="LVClass" URL="../bin/PPLs/Component Actors/Package Update Check Actor.lvlibp/Package Update Check Actor/Package Update Check Actor.lvclass"/>
					<Item Name="Space Constant.vi" Type="VI" URL="../bin/PPLs/Component Actors/Package Update Check Actor.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
					<Item Name="System Directory Type.ctl" Type="VI" URL="../bin/PPLs/Component Actors/Package Update Check Actor.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
					<Item Name="System Exec.vi" Type="VI" URL="../bin/PPLs/Component Actors/Package Update Check Actor.lvlibp/1abvi3w/vi.lib/Platform/system.llb/System Exec.vi"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../bin/PPLs/Component Actors/Package Update Check Actor.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../bin/PPLs/Component Actors/Package Update Check Actor.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
					<Item Name="Zyah Auto Update Utility.lvclass" Type="LVClass" URL="../bin/PPLs/Component Actors/Package Update Check Actor.lvlibp/1abvi3w/user.lib/Zyah Solutions/Zyah Auto Update Utility/Zyah Auto Update Utility.lvclass"/>
				</Item>
				<Item Name="Package Updater Actor.lvlibp" Type="LVLibp" URL="../bin/PPLs/Component Actors/Package Updater Actor.lvlibp">
					<Item Name="Messages for this Actor" Type="Folder"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../bin/PPLs/Component Actors/Package Updater Actor.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="Get Command Line Arguments.vi" Type="VI" URL="../bin/PPLs/Component Actors/Package Updater Actor.lvlibp/1abvi3w/vi.lib/Utility/Get Command Line Arguments.vi"/>
					<Item Name="Get System Directory.vi" Type="VI" URL="../bin/PPLs/Component Actors/Package Updater Actor.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
					<Item Name="NIPM_API.lvlib" Type="Library" URL="../bin/PPLs/Component Actors/Package Updater Actor.lvlibp/1abvi3w/vi.lib/National Instruments/NIPM API (Beta)/NIPM_API.lvlib"/>
					<Item Name="Package Updater Actor.lvclass" Type="LVClass" URL="../bin/PPLs/Component Actors/Package Updater Actor.lvlibp/Package Updater Actor/Package Updater Actor.lvclass"/>
					<Item Name="Space Constant.vi" Type="VI" URL="../bin/PPLs/Component Actors/Package Updater Actor.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
					<Item Name="System Directory Type.ctl" Type="VI" URL="../bin/PPLs/Component Actors/Package Updater Actor.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
					<Item Name="System Exec.vi" Type="VI" URL="../bin/PPLs/Component Actors/Package Updater Actor.lvlibp/1abvi3w/vi.lib/Platform/system.llb/System Exec.vi"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../bin/PPLs/Component Actors/Package Updater Actor.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../bin/PPLs/Component Actors/Package Updater Actor.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
					<Item Name="Zyah Auto Update Utility.lvclass" Type="LVClass" URL="../bin/PPLs/Component Actors/Package Updater Actor.lvlibp/1abvi3w/user.lib/Zyah Solutions/Zyah Auto Update Utility/Zyah Auto Update Utility.lvclass"/>
				</Item>
			</Item>
		</Item>
		<Item Name="GLA Summit Example App.lvlib" Type="Library" URL="../GLA Summit Example App/GLA Summit Example App.lvlib"/>
		<Item Name="Launch GLA Summit Example App.vi" Type="VI" URL="../Launch GLA Summit Example App.vi"/>
		<Item Name="Launch GLA Summit Example App Updater.vi" Type="VI" URL="../Launch GLA Summit Example App Updater.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
			</Item>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="GLA Summit Example EXE" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{B4F73126-ED63-4CF9-8E76-AE65F6ABF0A9}</Property>
				<Property Name="App_INI_GUID" Type="Str">{64174A35-D971-4068-8CE9-BDAB31D2BD26}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{27F897D3-8A10-4011-94AC-DDEE2BB14E44}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">GLA Summit Example EXE</Property>
				<Property Name="Bld_buildSpecName" Type="Str">GLA Summit Example EXE</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Builds/GLA Summit Example EXE</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{348A4AAB-7FC6-4319-B89F-BFDC1EF76529}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Bld_version.patch" Type="Int">3</Property>
				<Property Name="Destination[0].destName" Type="Str">GLA Summit Example.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Builds/GLA Summit Example EXE/NI_AB_PROJECTNAME.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Builds/GLA Summit Example EXE/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">Support Binaries</Property>
				<Property Name="Destination[2].path" Type="Path">/C/Builds/GLA Summit Example EXE/bin</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Source[0].itemID" Type="Str">{9D4D6CBB-127A-41FF-B701-453F51B3B6D9}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Launch GLA Summit Example App.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/bin (PPLs)</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_companyName" Type="Str">Zyah Solutions</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GLA Summit Example</Property>
				<Property Name="TgtF_internalName" Type="Str">GLA Summit Example</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2022 Zyah Solutions</Property>
				<Property Name="TgtF_productName" Type="Str">GLA Summit Example</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{7D1E7B00-7CAE-4F34-BEF5-5EA050D2F534}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GLA Summit Example.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GLA Summit Example Package" Type="{E661DAE2-7517-431F-AC41-30807A3BDA38}">
				<Property Name="NIPKG_addToFeed" Type="Bool">true</Property>
				<Property Name="NIPKG_allDependenciesToFeed" Type="Bool">false</Property>
				<Property Name="NIPKG_allDependenciesToSystemLink" Type="Bool">false</Property>
				<Property Name="NIPKG_certificates" Type="Bool">true</Property>
				<Property Name="NIPKG_createInstaller" Type="Bool">false</Property>
				<Property Name="NIPKG_feedLocation" Type="Path">/C/Builds/GLA Summit Package Feed</Property>
				<Property Name="NIPKG_installerArtifacts" Type="Str"></Property>
				<Property Name="NIPKG_installerBuiltBefore" Type="Bool">false</Property>
				<Property Name="NIPKG_installerDestination" Type="Path">../builds/NI_AB_PROJECTNAME/GLA Summit Example Package/Package Installer</Property>
				<Property Name="NIPKG_installerDestination.Type" Type="Str">relativeToCommon</Property>
				<Property Name="NIPKG_lastBuiltPackage" Type="Str">gla-summit-example_1.0.3-0_windows_all.nipkg</Property>
				<Property Name="NIPKG_license" Type="Ref"></Property>
				<Property Name="NIPKG_packageVersion" Type="Bool">false</Property>
				<Property Name="NIPKG_releaseNotes" Type="Str">Initial release with fixed package name and feed issues</Property>
				<Property Name="NIPKG_storeProduct" Type="Bool">false</Property>
				<Property Name="NIPKG_VisibleForRuntimeDeployment" Type="Bool">false</Property>
				<Property Name="PKG_actions.Count" Type="Int">0</Property>
				<Property Name="PKG_autoIncrementBuild" Type="Bool">true</Property>
				<Property Name="PKG_autoSelectDeps" Type="Bool">true</Property>
				<Property Name="PKG_buildNumber" Type="Int">1</Property>
				<Property Name="PKG_buildSpecName" Type="Str">GLA Summit Example Package</Property>
				<Property Name="PKG_dependencies.Count" Type="Int">2</Property>
				<Property Name="PKG_dependencies[0].Enhanced" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[0].MaxVersion" Type="Str"></Property>
				<Property Name="PKG_dependencies[0].MaxVersionInclusive" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[0].MinVersion" Type="Str"></Property>
				<Property Name="PKG_dependencies[0].MinVersionType" Type="Str">Inclusive</Property>
				<Property Name="PKG_dependencies[0].Project.ID" Type="Ref">/My Computer/Build Specifications/GLA Summit Example Updater Package</Property>
				<Property Name="PKG_dependencies[0].Relationship" Type="Str">Required Dependency</Property>
				<Property Name="PKG_dependencies[0].Type" Type="Str">Project</Property>
				<Property Name="PKG_dependencies[1].Enhanced" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[1].MaxVersion" Type="Str"></Property>
				<Property Name="PKG_dependencies[1].MaxVersionInclusive" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[1].MinVersion" Type="Str">20.1.1.49157-0+f5</Property>
				<Property Name="PKG_dependencies[1].MinVersionType" Type="Str">Inclusive</Property>
				<Property Name="PKG_dependencies[1].NIPKG.DisplayName" Type="Str">LabVIEW Runtime (32-bit)</Property>
				<Property Name="PKG_dependencies[1].Package.Name" Type="Str">ni-labview-2020-runtime-engine-x86</Property>
				<Property Name="PKG_dependencies[1].Package.Section" Type="Str">Programming Environments</Property>
				<Property Name="PKG_dependencies[1].Package.Synopsis" Type="Str">The LabVIEW Runtime is a software add-on that enables engineers to run executables on a nondevelopment machine.</Property>
				<Property Name="PKG_dependencies[1].Relationship" Type="Str">Required Dependency</Property>
				<Property Name="PKG_dependencies[1].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_description" Type="Str">GLA Summit Example for Auto Update Utility</Property>
				<Property Name="PKG_destinations.Count" Type="Int">2</Property>
				<Property Name="PKG_destinations[0].ID" Type="Str">{695B90E2-46F8-4E01-B075-AAE459BEE547}</Property>
				<Property Name="PKG_destinations[0].Subdir.Directory" Type="Str">GLA Summit</Property>
				<Property Name="PKG_destinations[0].Subdir.Parent" Type="Str">root_5</Property>
				<Property Name="PKG_destinations[0].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[1].ID" Type="Str">{FB3A2C88-E85F-4F51-8DEC-34AC190CDCB7}</Property>
				<Property Name="PKG_destinations[1].Subdir.Directory" Type="Str">Example</Property>
				<Property Name="PKG_destinations[1].Subdir.Parent" Type="Str">{695B90E2-46F8-4E01-B075-AAE459BEE547}</Property>
				<Property Name="PKG_destinations[1].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_displayName" Type="Str">GLA Summit Example</Property>
				<Property Name="PKG_displayVersion" Type="Str">1.0.3</Property>
				<Property Name="PKG_feedDescription" Type="Str"></Property>
				<Property Name="PKG_feedName" Type="Str"></Property>
				<Property Name="PKG_homepage" Type="Str">https://www.zyahsolutions.com</Property>
				<Property Name="PKG_hostname" Type="Str"></Property>
				<Property Name="PKG_maintainer" Type="Str">Zyah Solutions &lt;info@zyahsolutions.com&gt;</Property>
				<Property Name="PKG_output" Type="Path">/C/Builds/GLA Summit Example Package</Property>
				<Property Name="PKG_packageName" Type="Str">gla-summit-example</Property>
				<Property Name="PKG_publishToSystemLink" Type="Bool">false</Property>
				<Property Name="PKG_section" Type="Str">Application Software</Property>
				<Property Name="PKG_shortcuts.Count" Type="Int">1</Property>
				<Property Name="PKG_shortcuts[0].Destination" Type="Str">root_1</Property>
				<Property Name="PKG_shortcuts[0].Name" Type="Str">GLA Summit Example</Property>
				<Property Name="PKG_shortcuts[0].Path" Type="Path"></Property>
				<Property Name="PKG_shortcuts[0].Target.Child" Type="Str">{7D1E7B00-7CAE-4F34-BEF5-5EA050D2F534}</Property>
				<Property Name="PKG_shortcuts[0].Target.Destination" Type="Str">{FB3A2C88-E85F-4F51-8DEC-34AC190CDCB7}</Property>
				<Property Name="PKG_shortcuts[0].Target.Source" Type="Ref">/My Computer/Build Specifications/GLA Summit Example EXE</Property>
				<Property Name="PKG_shortcuts[0].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_sources.Count" Type="Int">1</Property>
				<Property Name="PKG_sources[0].Destination" Type="Str">{FB3A2C88-E85F-4F51-8DEC-34AC190CDCB7}</Property>
				<Property Name="PKG_sources[0].ID" Type="Ref">/My Computer/Build Specifications/GLA Summit Example EXE</Property>
				<Property Name="PKG_sources[0].Type" Type="Str">EXE Build</Property>
				<Property Name="PKG_synopsis" Type="Str">GLA Summit Example for Auto Update Utility</Property>
				<Property Name="PKG_version" Type="Str">1.0.3</Property>
			</Item>
			<Item Name="GLA Summit Example Updater EXE" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{636673BA-48F5-46D6-8901-C68091B89920}</Property>
				<Property Name="App_INI_GUID" Type="Str">{71611F79-E586-4A56-B3BB-86DA65022E91}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{CB3CD494-38CE-4328-9A61-4EE7C7B6A196}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">GLA Summit Example Updater EXE</Property>
				<Property Name="Bld_buildSpecName" Type="Str">GLA Summit Example Updater EXE</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Builds/GLA Summit Example Updater EXE</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{547DC69D-B95A-4A10-A158-B8A809AC7B2E}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">GLA Summit Example Updater.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Builds/GLA Summit Example Updater EXE/GLA Summit Example Updater.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Builds/GLA Summit Example Updater EXE/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">Support Binaries</Property>
				<Property Name="Destination[2].path" Type="Path">/C/Builds/GLA Summit Example Updater EXE/bin</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{5A2F6A63-10F0-4551-968C-A7B3FB053444}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Launch GLA Summit Example App Updater.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/bin (PPLs)</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_companyName" Type="Str">Zyah Solutions</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GLA Summit Example Updater</Property>
				<Property Name="TgtF_internalName" Type="Str">GLA Summit Example Updater</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2022 Zyah Solutions</Property>
				<Property Name="TgtF_productName" Type="Str">GLA Summit Example Updater</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{FA1DF889-5B9D-4568-BB6F-4AFE266F7C8A}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">GLA Summit Example Updater.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="GLA Summit Example Updater Package" Type="{E661DAE2-7517-431F-AC41-30807A3BDA38}">
				<Property Name="NIPKG_addToFeed" Type="Bool">true</Property>
				<Property Name="NIPKG_allDependenciesToFeed" Type="Bool">false</Property>
				<Property Name="NIPKG_allDependenciesToSystemLink" Type="Bool">false</Property>
				<Property Name="NIPKG_certificates" Type="Bool">true</Property>
				<Property Name="NIPKG_createInstaller" Type="Bool">false</Property>
				<Property Name="NIPKG_feedLocation" Type="Path">/C/Builds/GLA Summit Package Feed</Property>
				<Property Name="NIPKG_installerArtifacts" Type="Str"></Property>
				<Property Name="NIPKG_installerBuiltBefore" Type="Bool">false</Property>
				<Property Name="NIPKG_installerDestination" Type="Path">../builds/NI_AB_PROJECTNAME/GLA Summit Example Package/Package Installer</Property>
				<Property Name="NIPKG_installerDestination.Type" Type="Str">relativeToCommon</Property>
				<Property Name="NIPKG_lastBuiltPackage" Type="Str">gla-summit-example-updater_1.0.0-0_windows_all.nipkg</Property>
				<Property Name="NIPKG_license" Type="Ref"></Property>
				<Property Name="NIPKG_packageVersion" Type="Bool">false</Property>
				<Property Name="NIPKG_releaseNotes" Type="Str">Initial release</Property>
				<Property Name="NIPKG_storeProduct" Type="Bool">false</Property>
				<Property Name="NIPKG_VisibleForRuntimeDeployment" Type="Bool">false</Property>
				<Property Name="PKG_actions.Count" Type="Int">0</Property>
				<Property Name="PKG_autoIncrementBuild" Type="Bool">true</Property>
				<Property Name="PKG_autoSelectDeps" Type="Bool">true</Property>
				<Property Name="PKG_buildNumber" Type="Int">1</Property>
				<Property Name="PKG_buildSpecName" Type="Str">GLA Summit Example Updater Package</Property>
				<Property Name="PKG_dependencies.Count" Type="Int">1</Property>
				<Property Name="PKG_dependencies[0].Enhanced" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[0].MaxVersion" Type="Str"></Property>
				<Property Name="PKG_dependencies[0].MaxVersionInclusive" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[0].MinVersion" Type="Str">20.1.1.49157-0+f5</Property>
				<Property Name="PKG_dependencies[0].MinVersionType" Type="Str">Inclusive</Property>
				<Property Name="PKG_dependencies[0].NIPKG.DisplayName" Type="Str">LabVIEW Runtime (32-bit)</Property>
				<Property Name="PKG_dependencies[0].Package.Name" Type="Str">ni-labview-2020-runtime-engine-x86</Property>
				<Property Name="PKG_dependencies[0].Package.Section" Type="Str">Programming Environments</Property>
				<Property Name="PKG_dependencies[0].Package.Synopsis" Type="Str">The LabVIEW Runtime is a software add-on that enables engineers to run executables on a nondevelopment machine.</Property>
				<Property Name="PKG_dependencies[0].Relationship" Type="Str">Required Dependency</Property>
				<Property Name="PKG_dependencies[0].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_description" Type="Str">GLA Summit Example Updater for Auto Update Utility</Property>
				<Property Name="PKG_destinations.Count" Type="Int">2</Property>
				<Property Name="PKG_destinations[0].ID" Type="Str">{695B90E2-46F8-4E01-B075-AAE459BEE547}</Property>
				<Property Name="PKG_destinations[0].Subdir.Directory" Type="Str">GLA Summit</Property>
				<Property Name="PKG_destinations[0].Subdir.Parent" Type="Str">root_5</Property>
				<Property Name="PKG_destinations[0].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[1].ID" Type="Str">{FB3A2C88-E85F-4F51-8DEC-34AC190CDCB7}</Property>
				<Property Name="PKG_destinations[1].Subdir.Directory" Type="Str">Example</Property>
				<Property Name="PKG_destinations[1].Subdir.Parent" Type="Str">{695B90E2-46F8-4E01-B075-AAE459BEE547}</Property>
				<Property Name="PKG_destinations[1].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_displayName" Type="Str">GLA Summit Example Updater</Property>
				<Property Name="PKG_displayVersion" Type="Str">1.0.0</Property>
				<Property Name="PKG_feedDescription" Type="Str"></Property>
				<Property Name="PKG_feedName" Type="Str"></Property>
				<Property Name="PKG_homepage" Type="Str">https://www.zyahsolutions.com</Property>
				<Property Name="PKG_hostname" Type="Str"></Property>
				<Property Name="PKG_maintainer" Type="Str">Zyah Solutions &lt;info@zyahsolutions.com&gt;</Property>
				<Property Name="PKG_output" Type="Path">/C/Builds/GLA Summit Example Updater Package</Property>
				<Property Name="PKG_packageName" Type="Str">gla-summit-example-updater</Property>
				<Property Name="PKG_publishToSystemLink" Type="Bool">false</Property>
				<Property Name="PKG_section" Type="Str">Application Software</Property>
				<Property Name="PKG_shortcuts.Count" Type="Int">0</Property>
				<Property Name="PKG_shortcuts[0].Destination" Type="Str">root_1</Property>
				<Property Name="PKG_shortcuts[0].Name" Type="Str">GLA Summit Example</Property>
				<Property Name="PKG_shortcuts[0].Path" Type="Path"></Property>
				<Property Name="PKG_shortcuts[0].Target.Child" Type="Str">{7D1E7B00-7CAE-4F34-BEF5-5EA050D2F534}</Property>
				<Property Name="PKG_shortcuts[0].Target.Destination" Type="Str">{FB3A2C88-E85F-4F51-8DEC-34AC190CDCB7}</Property>
				<Property Name="PKG_shortcuts[0].Target.Source" Type="Ref">/My Computer/Build Specifications/GLA Summit Example EXE</Property>
				<Property Name="PKG_shortcuts[0].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_sources.Count" Type="Int">1</Property>
				<Property Name="PKG_sources[0].Destination" Type="Str">{FB3A2C88-E85F-4F51-8DEC-34AC190CDCB7}</Property>
				<Property Name="PKG_sources[0].ID" Type="Ref">/My Computer/Build Specifications/GLA Summit Example Updater EXE</Property>
				<Property Name="PKG_sources[0].Type" Type="Str">EXE Build</Property>
				<Property Name="PKG_synopsis" Type="Str">GLA Summit Example Updater for Auto Update Utility</Property>
				<Property Name="PKG_version" Type="Str">1.0.0</Property>
			</Item>
		</Item>
	</Item>
</Project>
